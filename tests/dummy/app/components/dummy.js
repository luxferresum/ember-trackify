import Component from '@glimmer/component';
import trackify from 'ember-trackify';
import { action } from '@ember/object';

export default class DummyComponent extends Component {
  constructor() {
    super(...arguments);
    this.data = { foo: 10 };
    trackify(this.data, 'foo');
  }

  @action
  incr() {
    this.data.foo++;
  }
}
