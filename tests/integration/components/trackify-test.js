import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | trackify', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {

    await render(hbs`<Dummy />`);
    assert.dom('#data-foo').hasText('10');
    await click('#incr');
    assert.dom('#data-foo').hasText('11');
  });
});
